import {baseUrl, http} from "src/api/service";
import axios from "axios";
import {appendEditForm, appendForm} from "src/helpers/commonFunctions"
import appStorage from "src/helpers/appStorage";
import {fasSadCry} from "@quasar/extras/fontawesome-v5";


const state = () => ({
  vacancies: {},
  fetchingVacancies: false,
  addingVacancy: false,
});

const getters = {
  GET_FETCHING_VACANCIES: state => state.fetchingVacancies,
  GET_VACANCIES: state => state.vacancies,
  GET_ADDING_VACANCY: state => state.addingVacancy,
}

const mutations = {
  SET_FETCHING_VACANCIES(state, value) {
    state.fetchingVacancies = value
  },

  SET_VACANCIES(state, value) {
    state.vacancies = value
  },

  SET_ADDING_VACANCY(state, value) {
    state.addingVacancy = value
  },

}

const actions = {
  ADD_VACANCY(context, form) {
    context.commit('SET_ADDING_VACANCY', true)

    http.post(baseUrl + 'addVacancy', appendForm(form))
      .then(({data}) => {
        context.commit('SET_ADDING_VACANCY', false)
        let alert = {
          type: 'positive',
          message: 'Vacant Post Added Successful',
          position: 'top'
        }
        context.commit('SET_NOTIFICATION', alert, {root: true})

      })
      .catch(error => {
        context.commit('SET_ADDING_VACANCY', false)
        let alert = {

          type: 'negative',
          message: 'Error adding vacant post',
          position: 'top'
        }
        if (error.response && error.response.data && error.response.data.message) {
          alert.message = error.response.data.message;
        }
        context.commit('SET_NOTIFICATION', alert, {root: true})
        if (error.response && error.response.data && error.response.data.errors) {
          const errors = Object.values(error.response.data.errors)
          context.commit('SET_ERRORS', errors, {root: true})
        }
      })
  },

  FETCH_VACANCIES(context, filters) {
    context.commit('SET_FETCHING_VACANCIES', true)
    http.get(`vacancies?page=${filters.page} &filters=${JSON.stringify(filters)}`)
      .then(({data}) => {
        context.commit('SET_FETCHING_VACANCIES', false)
        context.commit('SET_VACANCIES', data)
      })
      .catch(error => {
        context.commit('SET_FETCHING_VACANCIES', false)
        let alert = {
          type: 'negative',
          message: 'Failed to load vacancies',
          position: 'top'
        }
        if (error.response && error.response.data && error.response.data.message) {
          alert.message = error.response.data.message;
        }
        context.commit('SET_NOTIFICATION', alert, {root: true})
      })
  },

  EDIT_VACANCY(context, form) {
    context.commit('SET_ADDING_VACANCY', true)

    http.post(baseUrl + 'editVacancy/' + form.id, appendEditForm(form))
      .then(({data}) => {
        context.commit('SET_ADDING_VACANCY', false)
        let alert = {
          type: 'positive',
          message: 'Edit Vacancy Successful',
          position: 'top'
        }
        context.commit('SET_NOTIFICATION', alert, {root: true})

      })
      .catch(error => {
        context.commit('SET_ADDING_VACANCY', false)
        let alert = {
          type: 'negative',
          message: 'Edit Unsuccessful',
          position: 'top'
        }
        if (error.response && error.response.data && error.response.data.message) {
          alert.message = error.response.data.message;
        }
        context.commit('SET_NOTIFICATION', alert, {root: true})
        if (error.response && error.response.data && error.response.data.errors) {
          const errors = Object.values(error.response.data.errors)
          context.commit('SET_ERRORS', errors, {root: true})
        }
      })
  },

  DELETE_VACANCY(context, id) {
    context.commit('SET_ADDING_VACANCY', true)

    http.delete(baseUrl + 'deleteVacancy/' + id)
      .then(({data}) => {
        context.commit('SET_ADDING_VACANCY', false)
        let alert = {
          type: 'positive',
          message: 'Delete Successful',
          position: 'top'
        }
        context.commit('SET_NOTIFICATION', alert, {root: true})
        context.commit('SET_ADDING_VACANCY', false)

      })
      .catch(error => {
        context.commit('SET_ADDING_VACANCY', false)
        let alert = {
          type: 'negative',
          message: 'Delete Unsuccessful',
          position: 'top'
        }
        if (error.response && error.response.data && error.response.data.message) {
          alert.message = error.response.data.message;
        }
        context.commit('SET_NOTIFICATION', alert, {root: true})
      })
  }


};


export default {
  state,
  getters,
  mutations,
  actions
}
