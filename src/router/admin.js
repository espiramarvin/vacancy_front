export default [
  {
    path: "admin",
    meta: { requiresAuth: true, roles: 'admin'},
    name: "AdminLayout",
    redirect: {
      name: "AdminPage"
    }
  },
  {
    path: "admin",
    meta: { requiresAuth: true, roles: 'admin' },
    name: "AdminPage",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        `pages/admin/ManageUsers.vue`
        )
  },
  {
    path: "users",
    meta: { requiresAuth: true, roles: 'admin' },
    name: "ManageUsers",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        `pages/admin/ManageUsers.vue`
        )
  },
  {
    path: "vacancies",
    meta: { requiresAuth: true, roles: 'admin' },
    name: "VacancyPage",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        `pages/admin/Vacancies.vue`
        )
  },
/*  {
    path: "admin",
    meta: { requiresAuth: true, roles: 'admin' },
    name: "AdminPage",
    component: () =>
      import(
        /!* webpackChunkName: "routes" *!/
        `pages/dashboard/Admin.vue`
        )
  },*/
];
